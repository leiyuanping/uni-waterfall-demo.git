# 用法说明

如果只是h5 + app ，可以直接使用插件， 如需兼容h5+app+微信小程序，参考/pages/water/index.vue页面

``` javascript
	
	<view class="content">
		<lyp-waterfall :column="3" :loadDataCount="loadDataCount" :dataList="list" ></lyp-waterfall>
	</view>

	import lypWaterfall from '@/components/lyp-waterfall/index.vue';
	
	export default {
		components:{
			lypWaterfall
		},
		data() {
			return {
				loading: 0,
				loadDataCount: 10,
				list: []
			}
		},
		onReady() {
			this.ajax()
		},
		// 加载更多数据
		onReachBottom() {
			setTimeout(() => {
				let list = randomData();
				this.loadDataCount = list.length;
				this.list = this.list.concat(list);
			},500)
		},
		methods: {
			ajax(){
				// 这里是请求获取到的数据
				let list = randomData();
				// 这里是请求获取到的数据的长度，注意不是全部数据的长度，而是当前获取的长度
				this.loadDataCount = list.length;
				
				this.list = list;
			}
		}
	}
```


组件 ： @/components/lyp-waterfall/index.vue
这个组件是瀑布流布局的计算页面，在这个页面里计算了每个view的定位left 和 top值。
组件 ： @/components/lyp-waterfall/item.vue
这个组件是你需要的列表布局，你可以在这里修改，把他布局成你所期望的布局，当然需要注意的是里面有获取节点信息的函数，如果类名改变，需要在函数里把类名修改成对应类名
``` javascript
	createSelectorQuery.select('.l-module') // .l-module是本组件的根节点类名，把他修改成你所使用的根节点类名
```


# 属性说明

| 属性 |	类型	|	默认值	|	必填	|	说明	|
|--	|--	|--	|--	|--	|
| dataList |	Array	|	[]	|	true	|	用户请求到的需要渲染页面的数据	|
| loadDataCount |	Number	|	10	|	true	|	请求后添加的数据数量,例如：分页1请求的数据是10条数据,那么loadDataCount=10,分页2请求的数据只有7条,那么loadDataCount=7	|
| column |	Number	|	2	|	false	|	瀑布流的列数	|
| padding |	Array	|	[12, 12]	|	false	|	左右两边的间距 0下标左1下标右 单位px	|
| spaceX |	Number	|	9	|	false	|	每一列之间的间距 单位px	|
| spaceY |	Number	|	9	|	false	|	每一行之间的间距 单位px	|



# 说明

希望能帮到你们，如果有问题或者建议加QQ 624428121。

插件可能存在性能问题，慎用。
